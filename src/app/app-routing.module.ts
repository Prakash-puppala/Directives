import { NgModule } from '@angular/core';
import {  ExtraOptions, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth.guard';
import { BuiltInDirectivesComponent } from './Components/built-in-directives/built-in-directives.component';
import { NgSwitchComponent } from './Components/built-in-directives/ng-switch/ng-switch.component';
import { CustomDirectivesComponent } from './Components/custom-directives/custom-directives.component';
import { PagenotfoundComponent } from './Components/pagenotfound/pagenotfound.component';
import { CustomPreloadingStrategyService } from './custom-preloading-strategy.service';

const routes: Routes = [
  {path:'built-in-directives',component:BuiltInDirectivesComponent,data:{auth:true},canActivate:[AuthGuard],
  children:[
    {
      path:'ng-switch',
      component:NgSwitchComponent
    }
  ]},
  
  {path:'custom-directives',component:CustomDirectivesComponent,data:{auth:false},canActivate:[AuthGuard]},
  { path: '',   redirectTo: '/built-in-directives', pathMatch: 'full',},
  { path: 'directive', loadChildren: () => import('./directive/directive.module').then(m => m.DirectiveModule),data:{preload:true}},
  {path:'**', component:PagenotfoundComponent},
];

const routerOptions: ExtraOptions = {
  scrollPositionRestoration: 'enabled',
  anchorScrolling: 'enabled',
  scrollOffset: [0, 85],
  preloadingStrategy:CustomPreloadingStrategyService,
};

@NgModule({
  imports: [RouterModule.forRoot(routes,routerOptions)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

